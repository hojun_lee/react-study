import React from 'react'; // react를 사용할 수 있도록 불러옵니다.
import Hello from './Hello'; // 파일명 Hello를 사용할 수 있도록 불러옵니다.

function App() {
  return (
    <div>
      <Hello />
      {/* Hello 라는 컴포넌트를 사용합니다. 사용할 땐 부모 요소가 있어야 사용할 수 있고 
      <div> 대신 react 에선 <Fragment>, <> 를 사용할 수 있어 의미없는 태그의 사용을 방지할 수 있습니다.*/}
    </div>
  );
}

export default App;