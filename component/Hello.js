import React from 'react';

function Hello() {
    return <div>Hello Would!</div>; // return 으로 <div> 를 반환합니다.
};

export default Hello; // Hello 라는  컴포넌트를 내보내 다른 컴포넌트에서 사용할 수 있도록 합니다.

// 위의 형태는 함수형 클래스 작성법으로 javascript 처럼 function 으로 시작하지만 이외에도 class 형태의 작성법이 있습니다.
// 함수형 컴포넌트는 function 대신 const 를 사용해 작성할 수도 있으며 작성법은 아래와 같습니다.

// import React from 'react';
 
// const Hello = () => {
//     return div>Hello Would!</div>;
// };
 
// export default Hello;