import React from 'react';

function Wrapper( {children} ) { 
    const style = {
        border : "3px solid black",
        padding : "20px", 
    };
    return (
        <div style={style}>{children}</div>
    )
}

export default Wrapper;

// 컴포넌트 태그 사이에 넣은 값을 조회하고 싶을 땐, props.children 을 조회하면 됩니다.