import React from 'react';
import Hello from './Hello';
import Wrapper from './Wrapper';

function App() {
  return (
    <Wrapper> 
      <Hello name="react" color="red"/>
      <Hello color="blue"/>
    </Wrapper>
  );
}

export default App;

// Wrapper 라는 컴포넌트 안에 Hello 컴포넌트를 넣은 경우
