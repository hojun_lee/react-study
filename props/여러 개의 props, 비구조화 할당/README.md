<h1>여러 개의 props, 비구조화 할당</h1> 

<h2>App.js</h2>

![props1](./image/props1.png)

<h2>Hello.js</h2>

![props2](./image/props2.png)

<h2>결과값</h2>

![props3](./image/props3.png)
