import React from 'react';

function Hello(props) { // props 를 객체 형태로 전달
    return <div>Hello Would!{props.name}</div> 
    // App.js의 name 값을 사용하기 위해 props.name 으로 조회
}

export default Hello;

// 비구조화 할당 문법을 활용해 보다 간결하게 작성할 수도 있습니다.

// function Hello({ color, name }) {
//   return <div style={{ color }}>Hello Would! {name}</div>
// }
