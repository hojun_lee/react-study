import React from 'react';

function Hello(props) { // props 를 객체 형태로 전달
    return <div style={{color: props.color}}>Hello Would!{props.name}</div> 
    // App.js의 name 값을 사용하기 위해 props.name 으로 조회
}

export default Hello;