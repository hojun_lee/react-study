import React from 'react';
import Hello from './Hello';

function App() {
  return (
    <div>
      <Hello name="react" color="red"/>
      <Hello color="blue"/>
    </div>
  );
}

export default App;

// prop 란 컴포넌트 속성을 설정할 때 사용하는 요소로 어떠한 값을 컴포넌트에게 전달해줘야 할 때 사용합니다.
